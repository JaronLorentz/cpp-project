#pragma once

#include <WinSock2.h>
#include <iostream>
using namespace std;

class ExchangeSubsystem
{
public:
	bool sendData(char sendbuf[], SOCKET handleSocket);
	bool recvData(SOCKET handleSocket, int isVMode);
};

