#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <thread>
#include <mutex>

using namespace std;

#pragma comment (lib,"ws2_32.lib")

#include <WinSock2.h>
#include "ExchangeSubsystem.h"
#include "ConnectionSubsystem.h"
#include "TestingSubsystem.h"

class SocketFacade {
public:

	SOCKET handleSocket;

	SocketFacade();

	void doConnection(PCSTR ip);
	void stopServer();
	void dropServer();
	void sendGrepData(char* sendbuf, int isVMode);


	thread tDoConnection(PCSTR ip);
	thread tStopServer();
	thread tdropServer();
	thread tSendGrepData(char* sendbuf, int isVMode);

private:
	ExchangeSubsystem* exchange;
	ConnectionSubsystem* connection;
	TestingSubsystem* testing;
};

