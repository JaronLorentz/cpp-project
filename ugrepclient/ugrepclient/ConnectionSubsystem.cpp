#include "ConnectionSubsystem.h"

SOCKET ConnectionSubsystem::connectServer(PCSTR ip)
{
	cout << "Attempting to connect to server with ip ( " << ip << " )." << endl;

	// AF_INET = socket can use IPV4, SOCK_STREAM = a connection-based protocol, IPPROTO_TCP = tcp socker.
	SOCKET handleSocket = 0;															// Creates a TCP socket to be used in connecting to server
	WSAData wsaData;																	// WSADATA contains Windows Sockets implementation information
	int wsaResult = WSAStartup(MAKEWORD(2, 2), &wsaData);								// The "MAKEWORD(2, 2)" tells the WSA to use version 2.2, the latest version listed on https://docs.microsoft.com/
	if (wsaResult == 0) {
		sockaddr_in serverAddress = { 0 };												// Create a var to hold the server address property 						
		serverAddress.sin_family = AF_INET;												// Set address to use IPV4
		serverAddress.sin_port = htons(PORT);											// Set address port

		handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		inet_pton(AF_INET, ip, &(serverAddress.sin_addr));								// Converts the serverAddress in "sockaddr_in" form to its numeric binary form		

		auto connected = connect(handleSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress));    // Attempting to connect to server

		if (connected == SOCKET_ERROR) {
			closesocket(handleSocket);
			WSACleanup();
		}
	}
	else {
		cout << "WSA Failed: Error Code -> " << wsaResult << endl;
	}
	return handleSocket;
}

void ConnectionSubsystem::disconnect(SOCKET handleSocket)
{
	cout << "Attempting to disconnect from server." << endl;
	char sendbuf[8] = "drop";
	if (exchange->sendData(sendbuf, handleSocket)) {
		cout << "Disconnect message sent." << endl;
	}
	else {
		cout << "Error disconnect message not sent." << endl;
	}
	closesocket(handleSocket);
	WSACleanup();
}

void ConnectionSubsystem::stopServer(SOCKET handleSocket)
{
	char sendbuf[8] = "stop";
	cout << "Attempting to stop server." << endl;
	if (exchange->sendData(sendbuf, handleSocket)) {
		cout << "Stop message sent." << endl;
	}
	else {
		cout << "Error stop message not sent." << endl;
	}
}
