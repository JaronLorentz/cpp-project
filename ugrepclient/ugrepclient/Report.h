#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>

using namespace std;

class Report
{
public:
private:
	int numOfMatches;														// Total # of matches
	string lineNumber;															// Line where string was found
	string filename;														// Name of file where name was found
	string line;
public:
	Report();
	void handleEntry(string& entry);
	void print();
	bool matches(Report lastReport);
};
