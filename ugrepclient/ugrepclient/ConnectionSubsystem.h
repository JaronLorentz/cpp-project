#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <thread>
#include <mutex>
#include "ExchangeSubsystem.h"

using namespace std;

#pragma comment (lib,"ws2_32.lib")

unsigned short constexpr PORT = 27015;

class ConnectionSubsystem
{
public:
	SOCKET connectServer(PCSTR ip);
	void disconnect(SOCKET handleSocket);
	void stopServer(SOCKET handleSocket);
private:
	ExchangeSubsystem* exchange;
};

