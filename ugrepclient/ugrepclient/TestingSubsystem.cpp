#include "TestingSubsystem.h"
		

bool TestingSubsystem::isValidIpv4(PCSTR ip)
{
	PCSTR reservedIpGS = "127.1.0.2";															// Reserverd Ip for grep and server
	PCSTR reservedIpCP = "127.1.0.3";															// Reserved Ip for Client and grep print window
	if (ip == reservedIpCP || ip == reservedIpGS) {
		cout << "Given ip address of ( " << ip << " ) is reserved for program";
	}
//	cout << "rest";
	struct sockaddr_in socketAddress;
	bool t = inet_pton(AF_INET, ip, &(socketAddress.sin_addr)) != 0;							// Testing that the Ip given is a valid Ipv4 address
	if (!t) {
		cout << "Given ip address of ( " << ip << " ) is not a valid IPV4 address." << endl;
	}
	return t;
}

bool TestingSubsystem::isConnected(SOCKET handleSocket)
{
	char recvbuf[1] = "";
	char sendbuf[1] = "";

	exchange->sendData(sendbuf, handleSocket);
	//send(handleSocket, sendbuf, 1, 0);

	if (recv(handleSocket, recvbuf, 1, 0) > 0) {
		cout << "Server is connected" << endl;
		return true;
	}
	else {
		cout << "Server is not connected" << endl;
		return false;
	}
}
