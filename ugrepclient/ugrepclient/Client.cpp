/*
 * Programmers: Jaron Lorentz, Bryce Hartwick
 * Date: 2019-11-16
 * Description: 
 */

#include <iostream>
#include <vector>
#include <sstream>
#include "SocketFacade.h"
#include "SafePrint.h"
#include <thread>


using namespace std;


mutex SafePrint::printMutex{};






/*
 * Name: split
 * Desc: Splits string into a vector via a delimenator.   ( TAKEN FROM P1 ) <- updated to take variables by reference
 * Returns none
 * Date: 2019-09-12
 */
void split(const string& str, char del, vector<string>& tks)
{
	tks.clear();
	string t;
	istringstream tStream(str);
	while (getline(tStream, t, del))
	{
		tks.push_back(t);
	}
}

/*
 * Name: doError
 * Desc: Prints error then exits.
 * Returns none
 * Date: 2019-11-17
 */
void doError(string err) {
	perror(err.c_str());
	exit(EXIT_FAILURE);
}


void printHelp() {
	SafePrint{} << "Client for project 3 by Jaron Lorentz and Bryce Hartwick." << endl << endl
		<< "User Interface:" << endl
		<< "drop          -> Drop connection to the server." << endl
		<< "stopserver    -> Disconnect and stop server/grep process." << endl
		<< "connect       -> Connect to server. format ( connect 127.0.0.1 )" << endl
		<< "grep          -> Run a grep command on the server. format ( grep -v . [Ss]tring .hpp.cpp )" << endl
		<< "help          -> Open this menu." << endl << endl;

}


int main(int argc, char* argv[]) {

	PCSTR userIp = "";																// Hold user chosen Ip
	string input;																	// Stores full line of user input
	vector<string> inputArgs;														// Stores user input divided by " " 
	SocketFacade* sFacade = new SocketFacade();										// Pointer to facade
	thread grepThread, stopThread, dropThread, connThread;
	vector<char> cVec;


	//The following are varients of usr input strings
	vector<string> drop_str = { "drop" , "disconnect" ,"d" };
	vector<string> stop_str = { "stopserver" , "stop" ,"end", "s" };
	vector<string> conn_str = { "connect" , "c" ,"conn" };
	vector<string> grep_str = { "grep" , "ultragrep" ,"g", "ug" };


	printHelp();

	//atexit(dropServer);

	for (;;) {
		//SafePrint{} << "-> ";
		input = "";
		getline(cin, input);
		if (input.empty()) {														// Breaks and ends program if user does not give any input
			//check for connection if so break connection
			break;
		}

		split(input, ' ', inputArgs);
		if (!inputArgs.empty()) {
			if (inputArgs.at(0) == "") {
				inputArgs.erase(inputArgs.cbegin());
			}
			else if (find(grep_str.begin(), grep_str.end(), inputArgs.at(0)) != grep_str.end()) {										    // The grep command has the same semantics as project 2
				int isVMode = 2;
				if (find(inputArgs.begin(), inputArgs.end(), "-v") != inputArgs.end())
				{
					inputArgs.erase(find(inputArgs.begin(), inputArgs.end(), "-v"));
					isVMode = 1;
				}
				vector<char> cVecT(input.c_str(), input.c_str() + input.size() + 1);
				cVec = cVecT;
				if (grepThread.joinable()) {
					grepThread.join();
				}
				cout << "sending data " << &cVec[0] << endl;
				grepThread = sFacade->tSendGrepData(&cVec[0], 1);

				//sFacade->sendGrepData(tmp);
			}
			else if (find(conn_str.begin(), conn_str.end(), inputArgs.at(0)) != conn_str.end()) {								// The connect command connects the client the server at the specified add
				if (inputArgs.size() > 1) {												// If user gave ip the store it in userIp
					userIp = inputArgs.at(1).c_str();
				}
				else {																	// Else use the default
					userIp = "127.0.0.1";
				}
				if (connThread.joinable()) {
					//SafePrint{} << "ip = " << userIp;
					connThread.join();
				}
				//SafePrint{} << "ip = " << userIp;
				connThread = sFacade->tDoConnection(userIp);

				//sFacade->doConnection(userIp);										// Call doConnection via the Socket Facade. 
			}
			else if (find(stop_str.begin(), stop_str.end(), inputArgs.at(0)) != stop_str.end()) {									// The stopserver command instructs the grep server to shutdo
				if (stopThread.joinable()) {
					stopThread.join();
				}
				if (!grepThread.joinable()) {
					grepThread.detach();
				}
				stopThread = sFacade->tStopServer();
				//sFacade->stopServer();
			}
			else if (find(drop_str.begin(), drop_str.end(), inputArgs.at(0)) != drop_str.end()) {										// The drop command disconnects from the serv
				if (dropThread.joinable()) {
					dropThread.join();
				}
				dropThread = sFacade->tdropServer();
				//sFacade->dropServer();
			}
			else if (inputArgs.at(0) == "help") {
				printHelp();
			}
			else {
				SafePrint{} << "Argument " << inputArgs.at(0) << " not valid." << endl;
			}
		}
	}

}

