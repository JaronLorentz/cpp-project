import{ Component, OnInit } from '@angular/core';
import{ FormControl, FormGroup, FormBuilder } from '@angular/forms';
import{ Vendor } from '../../vendor/vendor';
import{ RestfulService } from '../../restful.service';
import{ Product } from '../../product/product';
import{ ReportItem } from '../report-item';
import{ Report } from '../report';
import{ BASEURL, PDFURL } from '../../constants';
@Component({
templateUrl: './report-viewer.component.html'
})
export class ReportViewerComponent implements OnInit {
	// form
generatorForm: FormGroup;
vendorid: FormControl;
reportid: FormControl;

	// component
vendors: Array<Vendor>; // all vendors
vendorreports: Array<Report>; // all products for a particular vendor
products: Array<Product>;
selectedProducts: Array<Product>;
selectedReport: Report; // the current selected product
selectedVendor: Vendor; // the current selected vendor
pickedProduct: boolean;
pickedVendor: boolean;
generated: boolean;
hasReport: boolean;
msg: string;
total: number;
reportno: number;
url: string;
reports: Array<Report>;
	constructor(private builder: FormBuilder, private restService : RestfulService) {
		this.pickedVendor = false;
		this.pickedProduct = false;
		this.generated = false;
		this.url = BASEURL + 'reports';
	} // constructor
	ngOnInit() {
		this.vendorid = new FormControl('');
		this.reportid = new FormControl('');
		this.generatorForm = this.builder.group({
		  reportid: this.reportid,
		  vendorid : this.vendorid
			});
		this.onPickVendor();
		this.onPickReport();
		this.msg = 'loading vendors from server...';
		this.restService.load(BASEURL + 'vendors').subscribe(
			vendorPayload = > {
			this.vendors = vendorPayload._embedded.vendors;
			this.msg = 'vendors loaded';
			this.msg = 'loading reports from server...';
			this.restService.load(BASEURL + 'reports').subscribe(
				reportPayload = > {
				this.reports = reportPayload._embedded.reports;
				this.msg = 'server data loaded';
			},
				err = > {this.msg += `Error occurred - products not loaded - ${ err.status } -${ err.statusText }`;
			});
		},
			err = > {this.msg += ` Error occurred - vendors not loaded - ${ err.status } -${ err.statusText }`;
		});
		this.restService.load(BASEURL + 'products').subscribe(
			productPayload = > {
			this.products = productPayload._embedded.products;
			this.msg = 'server data loaded';
		},
			err = > {this.msg += `Error occurred - products not loaded - ${ err.status } -${ err.statusText }`;
		});
	} // ngOnInit

	/**
	 * onPickVendor - subscribe to the select change event then load specific
	 * vendor products for subsequent selection
	 */
	onPickVendor() : void {
		this.generatorForm.get('vendorid').valueChanges.subscribe(val = > {
			this.selectedReport = null;
			this.selectedVendor = val;
			this.loadVendorReports();
			this.pickedProduct = false;
			this.hasReport = false;
			this.msg = 'choose report for vendor';
			this.pickedVendor = true;
			this.generated = false;
		});
	}

	onPickReport() : void {
		this.generatorForm.get('reportid').valueChanges.subscribe(val = > {
			this.msg = "Report selected";
			if (this.pickedVendor) {
				this.hasReport = true;
				this.selectedReport = val;
				this.reportno = val.id;
				this.msg = val.items.name;
				this.selectedProducts = new Array<Product>();
				for (let entry of this.products) {
					for (let e of this.selectedReport.items) {
						if (entry.id == e.id) {
							this.selectedProducts.push(entry);
						}
					}
				}
			}
		});
	}

	loadVendorReports() {
		this.vendorreports = [];
		this.vendorreports = this.reports.filter(ex = > ex.vendorid == = this.selectedVendor.id);
		// filter products for single vendor
	} // loadVendorProducts


	/**
	 * viewPdf - determine report number and pass to server
	 * for PDF generation in a new window
	 */
	viewPdf() {
		window.open(PDFURL + this.reportno, '');
	} // viewPdf

} // ReportGeneratorComponent
