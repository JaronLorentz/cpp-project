#include "SocketFacade.h"

SOCKET handleSocket;


SocketFacade::SocketFacade()
{
	handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);									// Creates a TCP socket to be used in connecting to server
	SocketFacade::exchange = new ExchangeSubsystem();
	SocketFacade::connection = new ConnectionSubsystem();
	SocketFacade::testing = new TestingSubsystem();
}

void SocketFacade::doConnection(PCSTR ip)
{
	if (testing->isValidIpv4(ip) && !testing->isConnected(handleSocket)) {	   // Check if ip is valid and server is not already connected
		handleSocket = connection->connectServer(ip);
		bool t = testing->isConnected(handleSocket);
	}
}

void SocketFacade::stopServer()
{
	if (testing->isConnected(handleSocket)) {
		connection->stopServer(handleSocket);
	}
}

void SocketFacade::dropServer()
{
	if (testing->isConnected(handleSocket)) {
		connection->disconnect(handleSocket);
		testing->isConnected(handleSocket);
	}
}

void SocketFacade::sendGrepData(char sendbuf[], int isVMode)
{
	if (testing->isConnected(handleSocket)) {
		exchange->sendData(sendbuf, handleSocket);
		thread a;
		a = thread([=] { exchange->recvData(handleSocket, isVMode); });
		a.join();
		a.~thread();
	}
}


thread SocketFacade::tDoConnection(PCSTR ip)
{
	return std::thread([=] { doConnection(ip); });
}

thread SocketFacade::tStopServer()
{
	return std::thread([=] { stopServer(); });
}

thread SocketFacade::tdropServer()
{
	return std::thread([=] { dropServer(); });
}

thread SocketFacade::tSendGrepData(char* sendbuf,int isVMode)
{
	return std::thread([=] { sendGrepData(sendbuf,isVMode); });
}
