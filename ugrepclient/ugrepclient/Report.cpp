/*
 * Custom report object to print and save reports in.
 */
#include "Report.h"


	Report::Report() {																// Blank Constructor
		this->numOfMatches = 0;
		this->lineNumber = -1;
		this->filename = "";
		this->line = "";
	};

	// Not declared in header as this function will not be user outside of this file
	vector<string> split(const string& str, char del, vector<string> & data)
	{
		vector<string> tks;
		string t;
		istringstream tStream(str);
		while (getline(tStream, t, del))
		{
			tks.push_back(t);
		}
		return tks;
	}

	// Takes raw data from server then converts it into a report
	void Report::handleEntry(string& entry) {
		// First seperate entry into parts via split function
		vector<string> data = split(entry, '?', data);				// Using ? as deleminator as it is an illegal filename character
		// Now if data was read correctly pass these values into the report obj
		if (data.size() == 3) {							// Only set the report data if this is a new report otherwise just increment
			if (this->filename == "") {
				this->filename=data.at(0);
				this->lineNumber = data.at(1);
				this->line = data.at(2);
			}
		 this->numOfMatches++;
		}
	}


	void Report::print() {
		cout << "|" << " # of Matches -> " << numOfMatches << "|" << " Line # -> " << lineNumber << "|" << " Filename -> " << filename << "| --> " << line << endl;
	}

	bool Report::matches(Report lastReport) {
		if (this->filename == lastReport.filename && this->lineNumber == lastReport.lineNumber) {
			return true;
		}
		else {
			return false;
		}
	}