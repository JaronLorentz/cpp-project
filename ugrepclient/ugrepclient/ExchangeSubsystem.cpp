#include "ExchangeSubsystem.h"
#include "SafePrint.h"
#include <vector>
#include "Report.h"



	bool ExchangeSubsystem::sendData(char sendbuf[], SOCKET handleSocket){
		bool doPrint = false;
		if (!((sendbuf != NULL) && (sendbuf[0] == '\0'))) {
			doPrint = true;
			SafePrint{} << "Attempting to send data -> " << sendbuf << endl;
		}

			if (send(handleSocket, sendbuf, sizeof(sendbuf), 0) > 0) {
				if (doPrint) {
					SafePrint{} << "Grep data sent correctly." << endl;
				}
				return true;
			}
			else {
				if (doPrint) {
					SafePrint{} << "Error sending grep data." << endl;
				}
				return false;
			}
	}

	/* 
	 * isVMode = 1 --> print each line as sent + report at end
	 * isVMode = 2 --> print all lines at end as report 
	 */
	bool ExchangeSubsystem::recvData(SOCKET handleSocket, int isVMode) {
		vector<string> data;
		bool gotData = false, listenToServer = true;
		char recvbuf[128];
		char sendbuf[32] = "ksending";

		string toString = "";
		while(listenToServer) {
			if(recv(handleSocket, recvbuf, sizeof(recvbuf), 0)>0){		
				toString.assign(recvbuf);
				if (toString == "done") {
					listenToServer = false;
				}
				else if (sizeof(recvbuf) > 0) {
					send(handleSocket, sendbuf, sizeof(sendbuf), 0);
					if (isVMode == 1 && strlen(recvbuf) != 0) {
						SafePrint{} << "->" << recvbuf << endl;
					}
					data.push_back(toString);
					gotData = true;
				}
			}

		}
		// Saftey check incase the server failed to send report or report was garbled in some way
		if (data.size() >= 3) {
			if (data.at(data.size() - 1).find("*") == string::npos) {
				
				// Grab the last three items sent from server as this will be the report
				string summary = data.at(data.size() - 3) +
					"\n" + data.at(data.size() - 2) +
					"\n" + data.at(data.size() - 1);
				
				// Pop the report data from vec
				data.erase(data.cend()-3,data.cend());

			}
		}
		else {
			SafePrint{} << "Error reading final report. Report not printed!" << endl;
		}

		data.erase(data.cbegin());

		SafePrint{} << "Printing grep response from server. Total ( " << data.size() << " ) results found." << endl;

		vector<Report> reports;											// Vec to hold all the reports
		Report report, lastReport = Report();							
		for (string s : data) {
			report = Report();
			report.handleEntry(s);										// Create report of current entry
			if (report.matches(lastReport)) {							// If the last report matches the current in filename and line # replace with the current report
				reports.pop_back();										
			}
			reports.push_back(report);
		}
		for (Report r : reports) {
			r.print();
		}
		return gotData;
	}