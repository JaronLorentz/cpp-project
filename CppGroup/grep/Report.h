#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>

using namespace std;

class Report
{
public:
	int numOfMatches;														// Total # of matches
	string lineNumber;															// Line where string was found
	string filename;														// Name of file where name was found
	string line;

	Report();
	string handleEntry();
	void print();
};
