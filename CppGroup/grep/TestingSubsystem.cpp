#include "TestingSubsystem.h"


//bool TestingSubsystem::isValidIpv4(PCSTR ip)		// revoved as this is now hardcooded for the server grep relationship

bool TestingSubsystem::isConnected(SOCKET handleSocket)
{
	char recvbuf[1] = "";
	char sendbuf[1] = "";

	exchange->sendData(sendbuf, handleSocket);
	//send(handleSocket, sendbuf, 1, 0);

	if (recv(handleSocket, recvbuf, 1, 0) > 0) {
		cout << "Server is connected" << endl;
		return true;
	}
	else {
		cout << "Server is not connected" << endl;
		return false;
	}
}
