/*
 * Reworked for final project 2019-12-04
 */

/*
UltraGrep
Course INFO-5104 C++ Advanced Topics
Due November 8th, 2019
Weight 15%
Project Description: Create a C++ 17 application that recursively searches a files system for text files or code files and list all of the lines that contain a given regular expression.
The application�s interface is: ultragrep [-v] folder expr [extention-list]*
*/
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <queue>
#include <random>
#include <string>
#include <thread>
#include <vector>
#include <iostream>
#include <filesystem>
#include <future>
#include "Report.h"
#include <fstream>
#include "SafePrint.h"
#include <regex>
#include "SocketFacede.h"


using namespace std;
namespace fs = std::filesystem;

condition_variable wakeCondition;
condition_variable bCondition;
queue<unsigned> tasks;
mutex taskMutex;
mutex consoleMutex;
mutex wakeMutex;
mutex barrierMutex;
mutex mut;
mutex SafePrint::printMutex{};
unsigned numThreads = thread::hardware_concurrency();
unsigned bThreshold = numThreads + 1;
unsigned bCount = bThreshold;
unsigned bGeneration = 0;
bool isVMode = false;
bool mWork = true;
vector<string> keys;
vector<string> files;
vector<string> fileTypes;								// Vector of filetypes to searh for (Default -> .txt )
vector<Report> reports;
int total, fileTotals = 0;
auto sTime = chrono::steady_clock::now();

SocketFacade* sFacade = new SocketFacade();
thread dataTransferT;

/*
 * Name: split
 * Desc: Splits string into a vector via a delimenator.   ( TAKEN FROM MY P1 )
 * Returns vector of strings.
 * Date: 2019-09-12
 */
vector<string> split(const string& str, char del)
{
	vector<string> tks;
	string t;
	istringstream tStream(str);
	while (getline(tStream, t, del))
	{
		tks.push_back(t);
	}
	return tks;
}

void cpp11Barrier() {
	unique_lock<mutex> bLock(barrierMutex);
	unsigned gen = bGeneration;
	if (--bCount == 0) {
		++bGeneration;
		bCount = bThreshold;
		bCondition.notify_all();
		{lock_guard<mutex> lguard(consoleMutex); }
	}
	else {
		{lock_guard<mutex> lguard(consoleMutex); }
		while (gen == bGeneration)
			bCondition.wait(bLock);
	}
}


void performCpp11Search() {
	{
		lock_guard<mutex> lguard(consoleMutex);
	}
	cpp11Barrier();
	string line;
	int i = 0;
	Report report, reset;
	while (mWork) {
		{
			unique_lock<mutex> lguard(wakeMutex);
			wakeCondition.wait(lguard);
		}
		while (!tasks.empty()) {
			unsigned task = 0;
			bool hTask = false;
			{
				lock_guard<mutex> lguard(taskMutex);
				if (!tasks.empty()) {
					task = tasks.front();
					tasks.pop();
					hTask = true;
				}
			}
			if (hTask) {
				{ lock_guard<mutex> lguard(consoleMutex);
				SafePrint{} << "Grepping file " << files.at(task - 1) << endl;
				}
				int i = 0;
				try {
					ifstream inputfileStream(files.at(task - 1));
					report.filename = files.at(task - 1);
					reset.filename = files.at(task - 1);
					bool hadMatch = false;
					while (getline(inputfileStream, line))
					{
						line = regex_replace(line, regex("^\\s+"), string(""));
						i++;
						report = reset;
						for (string key : keys) {
							report.lineNumber = i;
							int keyL = (int)key.length();
							int lineL = (int)line.length();
							size_t t = line.find(key);
							report.line =line;
							while (t != string::npos) {
								hadMatch = true;
								report.numOfMatches;
								t = line.find(key, t + key.size());
							}
							if (isVMode && report.numOfMatches > 0) {
								string output = files.at(task - 1) + "?" + to_string(i) + "?" + line;
								SafePrint{} << output << endl;
								dataTransferT = sFacade->tSendGrepData(output);
							}
							mut.lock();
							if (report.numOfMatches > 0) {
								reports.push_back(report);
								total += report.numOfMatches;
							}
							mut.unlock();
						}
					}
					if (hadMatch) {
						fileTotals++;
					}
				}
				catch (exception e) {
					SafePrint{} << e.what();
				}
			}
		}

	}
	{lock_guard<mutex> lguard(consoleMutex); }
}

int main(int argc, char* argv[]) {

	// Addition
	cout << "Project 3 grep by Jaron Lorentz , Bryce Hartwick." << endl;

	PCSTR userIp = "127.1.0.2";											// Hold user chosen Ip
	string input, output;
	sFacade->doConnection(userIp);
	

	//Command tracking Vars
	string searchLoc;										// Search Dir (Default ->  . == CWD )
	vector<string> cla(argv + 1, argv + argc);
	keys.push_back("");

	if (cla.size() >= 2) {
		//SafePrint{} << cla.at(0) << endl;
		if (find(cla.begin(), cla.end(), "-v") != cla.end())	// Test for Vmode toggle
		{
			isVMode = true;										// Set Vmode and pop arg
			cla.erase(cla.cbegin(), cla.cbegin() + 1);			// Erase first element in vec
			output = "Now running in Verbose mode.";
			SafePrint{} << output << endl;
			dataTransferT = sFacade->tSendGrepData(output);
		}
		if (!cla.empty()) {
			searchLoc = cla.at(0);									// Set search location
			cla.erase(cla.cbegin(), cla.cbegin() + 1);				// Erase search location from vec
		}
		if (!cla.empty()) {
			if (!(find(cla.begin(), cla.end(), "[") != cla.end()))	    // If extra char are added
			{
				vector<vector<char>> keyTmp;
				vector<char> chartmp;
				bool isInBlock = false;
				for (char currentChar : cla.at(0))					// Loop through string
				{
					if (isInBlock) {
						if (currentChar == ']') {
							isInBlock = false;
							keyTmp.push_back(chartmp);
							chartmp.clear();
						}
						else {
							chartmp.push_back(currentChar);
						}
					}
					else if (currentChar == '[') {
						isInBlock = true;
					}
					else {
						chartmp.push_back(currentChar);
						keyTmp.push_back(chartmp);
						chartmp.clear();
					}
				}
				vector<string> tmpStrings;
				vector<string> tmpStringsMain;
				for (vector<char> vec : keyTmp) {
					if (vec.size() == 1) {
						for (unsigned i = 0; i < keys.size(); i++) {
							keys[i] += vec.at(0);
						}
					}
					else {
						for (char c : vec) {
							for (unsigned i = 0; i < keys.size(); i++) {
								tmpStrings.push_back(keys.at(i) + c);
							}
							tmpStringsMain.insert(tmpStringsMain.end(), tmpStrings.begin(), tmpStrings.end());
							tmpStrings.clear();
						}
						keys = tmpStringsMain;
						tmpStringsMain.clear();
					}
				}
			}
			else
			{
				keys.push_back(cla.at(0));
			}
			cla.erase(cla.cbegin(), cla.cbegin() + 1);				// Erase search location from vec
		}
		if (!cla.empty()) {
			fileTypes = split(cla.at(0), '.');						// Split ext types by deliminator . and add to file types vec
			fileTypes.erase(fileTypes.cbegin(), fileTypes.cbegin() + 1);
		}
		if (fileTypes.empty()) {									// If no file types where decoded set to default .txt
			fileTypes.push_back(".txt");
		}
		//search loc msg
		output = "Searching " + searchLoc;
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);
		//search types
		output = "for files with key(s) ";
		
		//print function
		for (auto s : keys) {
			output += s + "/";
		}
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);

		output = "by filetype(s) ";
		for (auto s : fileTypes) {
			output += s + "/";
		}
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);

		vector<thread> threads;
		for (unsigned i = 0; i < numThreads; ++i) {
			threads.push_back(thread(performCpp11Search));
		}
		cpp11Barrier();

		try {
			auto a = fs::recursive_directory_iterator(searchLoc);

			for (const auto& e : a) {
				if (!e.is_directory()) {
					for (string type : fileTypes) {
						if (e.path().string().find(type) != string::npos) {
							files.push_back(e.path().string());
						}
					}
				}
				else {
					SafePrint{} << "Scanning" << e.path() << endl;
				}
			}
			output = "Searching (" + to_string(files.size()) + ") file(s)";
			SafePrint{} << output << endl;
			dataTransferT = sFacade->tSendGrepData(output);
			
			for (unsigned iTask = 0; iTask < files.size();) {
				{lock_guard<mutex> lk(consoleMutex);
				++iTask;
				}
				{ lock_guard<mutex> lk(taskMutex);
				tasks.push(iTask); }
			}

			mWork = false;
			wakeCondition.notify_all();
			// cleanup
			for (auto& t : threads)
				t.join();
		}
		catch (exception e) {
			SafePrint{} << "Error opening file." << endl;
		}
		// Report search results
		if (!isVMode) {
			for (Report report : reports) {
				report.print();
			}
		}
		auto eTime = chrono::steady_clock::now();
		output = "*There was a total of " + to_string(total) + " matches  found.";
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);
		output = "*There was a total of " + to_string(fileTotals) + " files with matches.";
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);
		output = "*The program for a total time of " + to_string(chrono::duration_cast<chrono::milliseconds>(eTime - sTime).count()) + " microseconds.";
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);
	}
	else {
		output = "Insufficient arguments -->\nThe application's interface is: ultragrep [-v] folder expr [extention-list]";
		SafePrint{} << output << endl;
		dataTransferT = sFacade->tSendGrepData(output);
	}
}