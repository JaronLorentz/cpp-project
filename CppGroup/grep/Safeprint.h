#include <sstream>
#include <iostream>
#include <mutex>

using namespace std;

// A work around for printing data on threaded functions string , string
class SafePrint : public ostringstream {
public:
	SafePrint() {};
	~SafePrint() {
		lock_guard<mutex> g(printMutex);
		cout << this->str();
	}
private:
	static mutex printMutex;
};