#include "ExchangeSubsystem.h"
#include "SafePrint.h"
#include <vector>
#include "Report.h"



bool ExchangeSubsystem::sendData(char sendbuf[], SOCKET handleSocket) {
	bool doPrint = false;
	if (!((sendbuf != NULL) && (sendbuf[0] == '\0'))) {
		doPrint = true;
	}
	if (doPrint) {
		if (send(handleSocket, sendbuf, sizeof(sendbuf), 0) > 0) {
				SafePrint{} << "Sent: " << sendbuf << endl;
			return true;
		}
		else {
				SafePrint{} << "Error sending: " << sendbuf << endl;
			return false;
		}
	}
}

void ExchangeSubsystem::recvData(SOCKET handleSocket) {
	char recvbuf[32];
	string toString;
	for (;;) {
		if (recv(handleSocket, recvbuf, sizeof(recvbuf), 0) > 0) {
			toString.assign(recvbuf);
			if (toString == "done") {
				//terminate process

			}
		}
	}
}