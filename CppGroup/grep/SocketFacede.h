#pragma once

#include <iostream>
#include <thread>

#include "ExchangeSubsystem.h"
#include "ConnectionSubsystem.h"
#include "TestingSubsystem.h"

using namespace std;

#pragma comment (lib,"ws2_32.lib")

//unsigned short constexpr PORT = 27015;


class SocketFacade {
public:

	SOCKET handleSocket;

	SocketFacade();
	void doConnection(PCSTR ip);
	thread tSendGrepData(string sendbuf);

private:
	ExchangeSubsystem* exchange;
	ConnectionSubsystem* connection;
	TestingSubsystem* testing;
};