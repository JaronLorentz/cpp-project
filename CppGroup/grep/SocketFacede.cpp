#include "SocketFacede.h"

SocketFacade::SocketFacade()
{
	handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);						// Creates a TCP socket to be used in connecting to server
	exchange = new ExchangeSubsystem();
	connection = new ConnectionSubsystem();
	testing = new TestingSubsystem();
}

void SocketFacade::doConnection(PCSTR ip)
{
	if (!testing->isConnected(handleSocket)) {
			handleSocket = connection->connectServer(ip);
		}
}

thread SocketFacade::tSendGrepData(string sendbuf)
{
	char* data = sendbuf.data();
	return std::thread([=] { exchange->sendData(&data[0],this->handleSocket); });
}
