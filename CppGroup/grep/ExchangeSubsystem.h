#pragma once
/*
 * Programmers: Jaron Lorentz, Bryce Hartwick
 * Date: 2019-11-16
 * Description: This class handles all the functions that deal with the server socket and is wrapped in a facade.
 */
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
using namespace std;

class ExchangeSubsystem
{
public:
	bool sendData(char sendbuf[], SOCKET handleSocket);
	void recvData(SOCKET handleSocket);
};
