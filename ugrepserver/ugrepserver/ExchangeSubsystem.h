#pragma once
/*
 * Programmers: Jaron Lorentz, Bryce Hartwick
 * Date: 2019-11-16
 * Description: This class handles all the functions that deal with the server socket and is wrapped in a facade.
 */
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <thread>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <thread>
#include <filesystem>

using namespace std;

class ExchangeSubsystem
{
public:
	bool exec(const char* c, SOCKET handleSocket);

	void sendData(SOCKET handleSocket, char sendbuf[]);

	int recvData(SOCKET handleSocket);
};