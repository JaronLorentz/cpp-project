/*
 * Programmers: Jaron Lorentz, Bryce Hartwick
 * Date: 2019-11-16
 * Description: This class launches the socket server.
 */
#include <iostream>
#include "SocketFacade.h"
using namespace std;

int main(int argc, char** argv) {
	cout << "Project 3 server by Jaron Lorentz , Bryce Hartwick." << endl;

	PCSTR userIp = "127.0.0.1";											// Hold user chosen Ip
	string input;
	vector<string> inputArgs;											// Stores user input divided by " " 
	SocketFacade* sFacade = new SocketFacade();

	if (argc==1) {
		sFacade->doConnection(userIp);
	}
	else {
		PCSTR ip = argv[1];
		sFacade->doConnection(ip);
	}
}