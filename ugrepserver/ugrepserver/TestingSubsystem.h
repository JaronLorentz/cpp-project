#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string.h>

#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <thread>
#include <filesystem>
#include "ExchangeSubsystem.h"

class TestingSubsystem {
	public:
		bool isValidIpv4(PCSTR ip);
		bool isConnected(SOCKET handleSocket);
	
	};

