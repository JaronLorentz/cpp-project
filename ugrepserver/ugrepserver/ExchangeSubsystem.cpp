#include "ExchangeSubsystem.h"


bool ExchangeSubsystem::exec(const char c[], SOCKET handleSocket)
{
	char recvbuf[9] = "";
	char testbuf[9] = "ksending";
	bool wait = false;
	int breakout = 0;
	//	cout << "test here0" << c << endl;
	//unique_ptr<FILE, decltype(&_pclose)> p(_popen(c, "r"), _pclose);
	//if (!p) {
	//	throw runtime_error("ultragrep() failed!");
	//	return false;
	//}
	vector<string> d;
	d.push_back("Blue");
	d.push_back("Red");
	d.push_back("Orange");
	d.push_back("Yellow");
	d.push_back("Blue1");
	d.push_back("Red1");
	d.push_back("Orange1");
	d.push_back("Yellow1");
	d.push_back("Blue2");
	d.push_back("Red2");
	d.push_back("Orange2");
	d.push_back("Yellow2");
	d.push_back("Blue3");
	d.push_back("*REPORT1");
	d.push_back("*REPORT2");
	d.push_back("@REPORT3");

	string testA("ksending",9), testB, hld = "";
	for(string s : d){
		char* c = const_cast<char*>(s.c_str());
		sendData(handleSocket, c);
		cout << "( Sending ) - " << c << endl;
		wait = true;
		breakout = 0;
		hld = "";
		while (wait) {
			recv(handleSocket, recvbuf, strlen(c) + 1, 0);
			testB.assign(recvbuf, 9);
			if (testA.find(testB) == string::npos) {
				wait = false;
			}
			if (breakout > 15) {
				break;
			}
			breakout++;
			cout << "waiting on response... " << c << endl;
		}
	//	cout << "End data" << endl;
	}
	//cout << "Broken." << endl;

	char sendbuf[32] = "done";
	sendData(handleSocket, sendbuf);
	return true;
}

void ExchangeSubsystem::sendData(SOCKET handleSocket, char sendbuf[])
{
	send(handleSocket, sendbuf, strlen(sendbuf) + 1, 0);
}

int ExchangeSubsystem::recvData(SOCKET handleSocket)
{
	char recvbuf[256] = "";
	char sendbuf[256] = "";

	int  bytesRecieved = 0;
	for (;;) {
		send(handleSocket, sendbuf, strlen(sendbuf) + 1, 0);
		bytesRecieved = recv(handleSocket, recvbuf, 256, 0);
		char* a = recvbuf;
		string r = a, bufTest = "grep";
		if (r == "stop") {
			cout << "Stopping server." << endl;
			memset(recvbuf, 0, 255);
			closesocket(handleSocket);
			WSACleanup();
			exit(0);
		}
		else if (r == "drop") {
			cout << "Disconnecting server from client." << endl;
			memset(recvbuf, 0, 255);
			closesocket(handleSocket);
			WSACleanup();
			return 0;
		}
		else if (r == "testFromClient" || r == "") {
			memset(recvbuf, 0, 255);
		}
		else if(bufTest.find(r) != string::npos){

			memset(recvbuf, 0, 255);
			try {
				r = "C:\\Users\\jjlor\\Music\\ultragrep.exe -v ..\\..\\..\\";
				LPSTR str = const_cast<char*>(r.c_str());
				cout << "Attempting to run command " << r << endl;

				bool wait = exec(str, handleSocket);
				char a[5] = "done";
				sendData(handleSocket, a);
			}	
			catch (exception e) {
				cout << e.what();
			}
		}
	}
	return 0;
}

