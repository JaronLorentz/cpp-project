#include "SocketFacade.h"

SocketFacade::SocketFacade()
{
	handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);						// Creates a TCP socket to be used in connecting to server
	exchange = new ExchangeSubsystem();
	connection = new ConnectionSubsystem();
	testing = new TestingSubsystem();
}

void SocketFacade::doConnection(PCSTR ip)
{
	if (testing->isValidIpv4(ip) && !testing->isConnected(handleSocket)) {
		for (;;) {
			handleSocket = connection->connectServer(ip);
			int i = exchange->recvData(handleSocket);
			handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		}
	}
}
