#pragma once
/*
 * Programmers: Jaron Lorentz, Bryce Hartwick
 * Date: 2019-11-16
 * Description: This class handles all the functions that deal with the server socket and is wrapped in a facade.
 */
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string.h>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <thread>
#include <filesystem>
#include "ExchangeSubsystem.h"
#include "ConnectionSubsystem.h"
#include "TestingSubsystem.h"

using namespace std;

#pragma comment (lib,"ws2_32.lib")

unsigned short constexpr PORT = 27015;


class SocketFacade {
public:

	SOCKET handleSocket;

	SocketFacade();
	void doConnection(PCSTR ip);

private:
	ExchangeSubsystem* exchange;
	ConnectionSubsystem* connection;
	TestingSubsystem* testing;
};