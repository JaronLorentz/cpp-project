#include "ConnectionSubsystem.h"
#include "SocketFacade.h"

SOCKET ConnectionSubsystem::connectServer(PCSTR ip)
{
	{
		cout << "Attempting to connect to server with ip ( " << ip << " )." << endl;

		// AF_INET = socket can use IPV4, SOCK_STREAM = a connection-based protocol, IPPROTO_TCP = tcp socker.
		SOCKET handleSocket = 0;					// Creates a TCP socket to be used in connecting to server
		SOCKET handleAccepted = SOCKET_ERROR;
		WSAData wsaData;																	// WSADATA contains Windows Sockets implementation information
		int wsaResult = WSAStartup(MAKEWORD(2, 2), &wsaData);								// The "MAKEWORD(2, 2)" tells the WSA to use version 2.2, the latest version listed on https://docs.microsoft.com/
		if (wsaResult == 0) {
			sockaddr_in serverAddress = { 0 };												// Create a var to hold the server address property 						
			serverAddress.sin_family = AF_INET;												// Set address to use IPV4
			serverAddress.sin_port = htons(PORT);											// Set address port

			handleSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			inet_pton(AF_INET, ip, &(serverAddress.sin_addr));								// Converts the serverAddress in "sockaddr_in" form to its numeric binary form		

			if (bind(handleSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
				cerr << "Bind() failed" << endl;
				closesocket(handleSocket);
				WSACleanup();
			}
			else {
				cout << "TCP/IP socket bound.\n";

				if (listen(handleSocket, 1) == SOCKET_ERROR) {
					cerr << "Error listening on socket\n";
					closesocket(handleSocket);
					WSACleanup();
				}
				else {
					cout << "Waiting for connection\n";
					while (handleAccepted == SOCKET_ERROR) {
						handleAccepted = accept(handleSocket, NULL, NULL);
					}
					cout << "Client connected\n";
				}
			}
		}
		return handleAccepted;
	}
}
